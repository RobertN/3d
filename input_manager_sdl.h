#pragma once

#include <map>
#include "base/tasks/task_runner.h"
#include "input_manager.h"

class InputManagerSDL : public InputManager {
  public:
    InputManagerSDL(base::TaskRunner& task_runner);
    virtual ~InputManagerSDL();

    // Implementation of InputManager
    virtual void add_key_handler(int32_t key, std::function<void(void)>);
    virtual void add_event_handler(int32_t event, std::function<void(void)>);

  private:
    void handle_events();

    std::map<int32_t, std::function<void(void)>> key_mappings_;
    std::map<int32_t, std::function<void(void)>> event_mappings_;
    base::TaskRunner& task_runner_;
};
