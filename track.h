#pragma once

#include "render_object.h"

class Track : public RenderObject {
  public:
    Track(GLProgram* gl_program);
    ~Track();

    void generate_track();
};
