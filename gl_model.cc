#include "gl_model.h"

#include <limits>
#include <vector>
#include "third_party/vmmlib/include/vmmlib/vector.hpp"
#include "gl.h"
#include "gl_program.h"

using vmml::vec3f;

namespace {
const uint32_t kInvalidBufferId = std::numeric_limits<uint32_t>::max();
}

GLModel::GLModel(GLProgram* gl_program)
  : GLModel(gl_program,
	    std::vector<vec3f>(),
	    std::vector<GLuint>()) {
}

GLModel::GLModel(
    GLProgram* gl_program,
    const std::vector<vec3f>& vertices,
    const std::vector<GLuint>& indices)
  : gl_program_(gl_program)
  , indices_(indices)
  , vao_(kInvalidBufferId)
  , vb_(kInvalidBufferId)
  , ib_(kInvalidBufferId)
  , nb_(kInvalidBufferId)
  , tb_(kInvalidBufferId) {

  // Calculate normal for each triangle and add it to the vertex.
  // The result is normalized later. Not sure if this is correct =)
  std::vector<vec3f> normals(vertices.size());
  for (size_t i = 0; i < indices.size(); i+=3) {
    vec3f A = vertices[indices[i]];
    vec3f B = vertices[indices[i+1]];
    vec3f C = vertices[indices[i+2]];

    vec3f n;
    n.compute_normal(A, C, B);

    // Add all plane normals. All normals are normalized later.
    normals[indices[i]] += n;
    normals[indices[i+1]] += n;
    normals[indices[i+2]] += n;
  }

  for (const vec3f& v : vertices) {
    vertices_.push_back(v[0]);
    vertices_.push_back(v[1]);
    vertices_.push_back(v[2]);
  }

  for (vec3f& n : normals) {
    n.normalize();
    normals_.push_back(n[0]);
    normals_.push_back(n[1]);
    normals_.push_back(n[2]);
  }
  gl_init_buffers();
  gl_upload_data();
}

GLModel::~GLModel() {
  if (vao_ != kInvalidBufferId)
    glDeleteVertexArrays(1, &vao_);
  if (vb_ != kInvalidBufferId)
    glDeleteBuffers(1, &vb_);
  if (ib_ != kInvalidBufferId)
    glDeleteBuffers(1, &ib_);
  if (nb_ != kInvalidBufferId)
    glDeleteBuffers(1, &nb_);
  if (tb_ != kInvalidBufferId)
    glDeleteBuffers(1, &tb_);
}

void GLModel::draw() {
  glBindVertexArray(vao_);
  glBindBuffer(GL_ARRAY_BUFFER, vb_);
  GLuint in_Position = gl_program_->gl_loc_position();
  glVertexAttribPointer(in_Position, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(in_Position);

  glBindBuffer(GL_ARRAY_BUFFER, nb_);
  GLuint in_Normal = gl_program_->gl_normal_position();
  glVertexAttribPointer(in_Normal, 3, GL_FLOAT, GL_FALSE, 0, 0);
  glEnableVertexAttribArray(in_Normal);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib_);
  glDrawElements(GL_TRIANGLES, indices_.size(), GL_UNSIGNED_INT, 0L);
}

void GLModel::gl_init_buffers() {
  glGenVertexArrays(1, &vao_);
  glGenBuffers(1, &vb_);
  glGenBuffers(1, &ib_);
  glGenBuffers(1, &nb_);
}

void GLModel::gl_upload_data() {
  glBindVertexArray(vao_);

  glBindBuffer(GL_ARRAY_BUFFER, vb_);
  glBufferData(
      GL_ARRAY_BUFFER, vertices_.size() * sizeof(GLfloat),
      &vertices_.front(), GL_STATIC_DRAW);

  glBindBuffer(GL_ARRAY_BUFFER, nb_);
  glBufferData(
      GL_ARRAY_BUFFER, normals_.size() * sizeof(GLfloat),
      &normals_.front(), GL_STATIC_DRAW);

  glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ib_);
  glBufferData(
      GL_ELEMENT_ARRAY_BUFFER, indices_.size() * sizeof(GLuint),
      &indices_.front(), GL_STATIC_DRAW);
}
