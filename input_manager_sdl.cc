#include "input_manager_sdl.h"
#include <iostream>
#include <unordered_map>
#include <SDL2/SDL.h>
#include "base/tasks/task_runner.h"
#include "input_manager.h"

// static
InputManager* InputManager::Create(base::TaskRunner& task_runner) {
  return new InputManagerSDL(task_runner);
}

InputManagerSDL::InputManagerSDL(base::TaskRunner& task_runner)
  : task_runner_(task_runner) {
  task_runner_.post_task(std::bind(&InputManagerSDL::handle_events, this));
}

InputManagerSDL::~InputManagerSDL() {}

void InputManagerSDL::add_key_handler(
    int32_t key, std::function<void(void)> callback) {
  key_mappings_[key] = callback;
}

void InputManagerSDL::add_event_handler(
    int32_t event_type, std::function<void(void)> callback) {
  event_mappings_[event_type] = callback;
}

void InputManagerSDL::handle_events() {
  SDL_Event event;
  while (SDL_PollEvent(&event)) {
    if (event.type == SDL_KEYDOWN) {
      int32_t key = event.key.keysym.sym;
      if (key_mappings_.find(key) != key_mappings_.end()) {
        key_mappings_[key]();
      }
    } else if (event_mappings_.find(event.type) != event_mappings_.end()) {
      event_mappings_[event.type]();
    }
  }
  task_runner_.post_delayed_task(
      std::bind(&InputManagerSDL::handle_events, this),
      base::TaskRunner::Delay(500));
}
