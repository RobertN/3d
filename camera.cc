#include "camera.h"

#include <iostream>
#include "gl_program.h"
#include "input_manager.h"
#include "third_party/vmmlib/include/vmmlib/vector.hpp"

namespace {
vmml::mat4f look_at(vmml::vec3f p, vmml::vec3f l, vmml::vec3f v) {
  vmml::vec3f n, u;
  vmml::mat4f rot, trans;

  n = p - l;
  n.normalize();
  u.cross(v, n);
  u.normalize();
  v.cross(n, u);

  vmml::vector<4, float> row(u.x(), u.y(), u.z(), 0);
  rot.set_row(0, {u.x(), u.y(), u.z(), 0});
  rot.set_row(1, {v.x(), v.y(), v.z(), 0});
  rot.set_row(2, {n.x(), n.y(), n.z(), 0});
  rot.set_row(3, {0, 0, 0, 1});

  identity(trans);
  trans.set_translation(vmml::vec3f(-p.x(), -p.y(), -p.z()));
  return rot * trans;
}
}

Camera::Camera(std::shared_ptr<GLProgram> gl_program)
    : gl_program_(gl_program)
    , position_(0, 0, -10)
    , direction_(0, 0, 1)
    , up_(0, 1, 0)
    , left_(1, 0, 0)
    , rotation_y_(0) {
  identity(world_to_view_);
  update();
}

void Camera::update() {
  vmml::mat4f rotation;
  identity(rotation);
  rotation.rotate_y(rotation_y_);
  set_direction(rotation * direction_);
  rotation_y_ = 0;

  world_to_view_ = look_at(
      position_, position_ + direction_, up_);
  gl_program_->set_world_to_view_matrix(world_to_view_);
}

void Camera::move_forward() {
  position_ += direction_;
}

void Camera::move_backward() {
  position_ -= direction_;
}

void Camera::move_left() {
  position_ += left_;
}

void Camera::move_right() {
  position_ -= left_;
}

void Camera::rotate_left() {
  rotation_y_ += 0.1;
}

void Camera::rotate_right() {
  rotation_y_ -= 0.1;
}

void Camera::set_direction(const vmml::vec3f& direction) {
  direction_ = direction;
  left_.cross(up_, direction_);
  left_.normalize();
}
