#include "application.h"

#include <chrono>
#include <iostream>
#include <SDL2/SDL.h>
#include <memory>

#include "gl.h"
#include "gl_model.h"
#include "gl_program.h"
#include "input_manager.h"
#include "object.h"
#include "render_object.h"
#include "third_party/vmmlib/include/vmmlib/frustum.hpp"
#include "track.h"

namespace {

int32_t kKeyMoveForward = SDLK_w;
int32_t kKeyMoveBackward = SDLK_s;
int32_t kKeyMoveLeft = SDLK_a;
int32_t kKeyMoveRight = SDLK_d;
int32_t kKeyRotateLeft = SDLK_q;
int32_t kKeyRotateRight = SDLK_e;

class Scene : public Object {
 public:
  void on_traverse(TraverseObject& /*to*/) {}
};

Scene scene;
std::unique_ptr<RenderObject> o, o2;

}  // anonymous namespace

base::TaskRunner Application::task_runner_;

Application::Application()
  : width_(1024)
  , height_(768)
  , running_(true)
  , program_(GLProgram::Create(task_runner_))
  , input_manager_(InputManager::Create(task_runner_)) {
}

bool Application::init() {
  if (SDL_Init(SDL_INIT_VIDEO) < 0) {
    return false;
  }

  input_manager_->add_event_handler(
      SDL_QUIT, std::bind(&Application::terminate, this));
  input_manager_->add_key_handler(
      SDLK_ESCAPE, std::bind(&Application::terminate, this));

  window_.reset(SDL_CreateWindow(
      "3D", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
      width_, height_, SDL_WINDOW_OPENGL
  ), SDL_DestroyWindow);

  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 2);

  SDL_GL_CreateContext(window_.get());

  setup_opengl();

  // Temporary for having a simple "generated" GLModel being drawn.
  setup_track();

  o.reset(new RenderObject(program_.get()));
  o2.reset(new RenderObject(program_.get()));
  if (o->load_from("data/cessna.obj"))
    scene.add_child(o.get());
  if (o2->load_from("data/cessna.obj")) {
    o->add_child(o2.get());
    o2->scale(0.5);
    o2->move(vmml::vec3f(0.0, 50.0, 50.0));
  }

  program_->add_shader(GL_VERTEX_SHADER, "shader.vertex");
  program_->add_shader(GL_FRAGMENT_SHADER, "shader.fragment");
  program_->construct();

  camera_.reset(new Camera(program_));

  // Setup input event handlers.
  input_manager_->add_key_handler(
      kKeyMoveForward, std::bind(&Camera::move_forward, camera_.get()));
  input_manager_->add_key_handler(
      kKeyMoveBackward, std::bind(&Camera::move_backward, camera_.get()));
  input_manager_->add_key_handler(
      kKeyMoveLeft, std::bind(&Camera::move_left, camera_.get()));
  input_manager_->add_key_handler(
      kKeyMoveRight, std::bind(&Camera::move_right, camera_.get()));
  input_manager_->add_key_handler(
      kKeyRotateLeft, std::bind(&Camera::rotate_left, camera_.get()));
  input_manager_->add_key_handler(
      kKeyRotateRight, std::bind(&Camera::rotate_right, camera_.get()));

  return true;
}

void Application::run_frame() {
  // Schedule next frame
  if (running_) {
    task_runner_.post_delayed_task(
        std::bind(&Application::run_frame, this),
        base::TaskRunner::Delay(1000000 / 60));
  }

  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  camera_->update();

  // Set perspective
  vmml::frustumf f;
  f.set_perspective(3.14*2, width_/height_, 0.1, 1000.0);
  program_->set_projection_matrix(f.compute_matrix());

  track_->render();

  // Do stuff
  o2->rotate(0.01, normalize(vmml::vec3f(1.0, 1.0, 0.0)));

  TraverseObject to;
  to.enable_render(this);
  scene.traverse(to);

  SDL_GL_SwapWindow(window_.get());
}

static void rotate_object(Object* o, base::TaskRunner* tr) {
  o->rotate(0.1, normalize(vmml::vec3f(1.0, 1.0, 0.0)));
  tr->post_delayed_task(std::bind(rotate_object, o, tr),
      std::chrono::seconds(1));
}

void Application::main_loop() {
  task_runner_.post_task(std::bind(&Application::run_frame, this));
  task_runner_.post_delayed_task(std::bind(rotate_object, o.get(), &task_runner_),
				 std::chrono::seconds(1));
  task_runner_.run();
}

void Application::render(RenderObject* render_object, const vmml::mat4f& transform) {
  program_->set_model_to_world_matrix(transform);
  render_object->render();
}

void Application::setup_opengl() {
  glShadeModel(GL_SMOOTH);
  glCullFace(GL_BACK);
  glFrontFace(GL_CCW);
  glEnable(GL_CULL_FACE);
  glEnable (GL_DEPTH_TEST);
  glDepthFunc(GL_LESS);
  glClearColor(0.0f, 1.0f, 1.0f, 1.0f);
  glViewport(0, 0, width_, height_);
}

void Application::terminate() {
  task_runner_.shutdown();
  running_ = false;
}

void Application::setup_track() {
  track_.reset(new Track(program_.get()));
  track_->generate_track();
}
