#pragma once

#include <vector>
#include "third_party/vmmlib/include/vmmlib/vector.hpp"
#include "gl.h"
#include "gl_program.h"

class GLModel {
  public:
    GLModel(GLProgram* gl_program);
    GLModel(
        GLProgram* gl_program,
        const std::vector<vmml::vec3f>& vertices,
        const std::vector<uint32_t>& indices);
    virtual ~GLModel();

    void draw();

  private:
    void gl_init_buffers();
    void gl_upload_data();

    GLProgram* gl_program_;

    std::vector<GLfloat> vertices_;
    std::vector<GLfloat> normals_;
    std::vector<GLuint> indices_;

    uint32_t vao_, vb_, ib_, nb_, tb_;
};
