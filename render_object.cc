#include "render_object.h"

#include <iostream>
#include <string>
#include <vector>
#include "gl_model.h"
#include "gl_program.h"

RenderObject::RenderObject(GLProgram* gl_program)
  : gl_model_(nullptr)
  , gl_program_(gl_program) {
}

RenderObject::~RenderObject() {}

bool RenderObject::load_from(const std::string& filename) {
  std::string err;
  if (!tinyobj::LoadObj(&attrib_, &shapes_, &materials_,
        &err, filename.c_str())) {
    std::cerr << "Failed to load: " << filename << " because: " << err;
    return false;
  }

  std::vector<vmml::vec3f> vertices;
  for (uint32_t i = 0; i < attrib_.vertices.size(); i += 3) {
    vertices.push_back({
        attrib_.vertices[i],
        attrib_.vertices[i + 1],
        attrib_.vertices[i + 2]
    });
  }

  std::vector<uint32_t> indices;
  for (const tinyobj::shape_t& shape : shapes_) {
    for (const tinyobj::index_t& i : shape.mesh.indices) {
      indices.push_back(i.vertex_index);
    }
  }

  gl_model_.reset(new GLModel(gl_program_, vertices, indices));
  return true;
}

std::string RenderObject::debug_info() const {
  std::string res = "Object shapes:\n";
  for (auto& shape : shapes_)
    res += " " + shape.name + "\n";
  return res;
}

void RenderObject::render() {
  gl_model_->draw();
}

void RenderObject::on_traverse(TraverseObject& to) {
  if (to.render_delegate())
    to.render_delegate()->render(this, to.transform());
}
