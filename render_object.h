#ifndef RENDER_OBJECT_H_
#define RENDER_OBJECT_H_

#include <memory>
#include <string>
#include <vector>
#include "third_party/tinyobjloader/tiny_obj_loader.h"
#include "gl.h"
#include "object.h"

class GLModel;
class GLProgram;

class RenderObject : public Object {
public:
  RenderObject(GLProgram* gl_program);
  ~RenderObject();

  bool load_from(const std::string& filename);

  void render();

  std::string debug_info() const;

  // Object implementation
  void on_traverse(TraverseObject& to);

protected:
  std::unique_ptr<GLModel> gl_model_;
  GLProgram* gl_program_;

private:
  tinyobj::attrib_t attrib_;
  std::vector<tinyobj::shape_t> shapes_;
  std::vector<tinyobj::material_t> materials_;
};

#endif
