#include "track.h"

#include "third_party/vmmlib/include/vmmlib/vector.hpp"
#include "gl_model.h"
#include "gl_program.h"

Track::Track(GLProgram* gl_program)
  : RenderObject(gl_program) {
}

Track::~Track() {
}

void Track::generate_track() {
  std::vector<vmml::vec3f> vertices;
  std::vector<GLuint> indices;

  vertices.push_back(vmml::vec3f(0.0f, 0.0f, 0.0f));
  vertices.push_back(vmml::vec3f(0.0f, 1.0f, -10.5f));
  vertices.push_back(vmml::vec3f(3.5f, 0.0f, 0.f));
  vertices.push_back(vmml::vec3f(3.5f, 1.0f, -10.5f));

  indices.push_back(0);
  indices.push_back(2);
  indices.push_back(1);

  indices.push_back(2);
  indices.push_back(3);
  indices.push_back(1);

  gl_model_.reset(new GLModel(gl_program_, vertices, indices));
}
