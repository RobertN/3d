#include "task_runner.h"

#include <iostream>

#include <unistd.h> // linux

namespace base {

TaskRunner::Task::Task(std::function<void()> f) :
  f_(f) {
}

void TaskRunner::Task::run() const {
  f_();
}

TaskRunner::DelayedTask::DelayedTask(std::function<void()> f, Delay delay)
  : Task(f) {
  expires_ = Clock::now() + delay;
}

bool TaskRunner::DelayedTask::operator<(const DelayedTask& other) const {
  return other.expires_ < expires_; // smallest to biggest
}

void TaskRunner::post_task(const std::function<void()>& f) {
  tasks_.push(Task(f));
}

void TaskRunner::post_delayed_task(const std::function<void()>& f,
				  Delay delay) {
  delayed_tasks_.push(DelayedTask(f, delay));
}

void TaskRunner::run() {
  running_ = true;
  while (running_) {
    Delay delay = get_delay_until_next_task();
    if (delay > Delay::zero()) {
      std::chrono::microseconds us = delay;
      usleep(us.count());
    }
    if (!run_next_task())
      return;
  }
}

void TaskRunner::shutdown() {
  running_ = false;
}

bool TaskRunner::run_next_task() {
  // Run instant tasks first.
  if (tasks_.size() > 0) {
    tasks_.front().run();
    tasks_.pop();
    return true;
  }

  // Run the first delayed task. Assumes we have not been called until it's
  // time.
  if (delayed_tasks_.size() > 0) {
    delayed_tasks_.top().run();
    delayed_tasks_.pop();
    return true;
  }
  return false;
}

TaskRunner::Delay TaskRunner::get_delay_until_next_task() {
  if (tasks_.size() > 0)
    return Delay::zero();

  if (delayed_tasks_.size() > 0) {
    return std::chrono::duration_cast<Delay>(
        delayed_tasks_.top().expires() - Clock::now());
  }

  // This is ok. The sleep will be skipped and the run() call
  // will end instantly.
  return Delay::zero();
}

}
