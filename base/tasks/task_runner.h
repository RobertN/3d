#ifndef TASKS_TASK_RUNNER_H
#define TASKS_TASK_RUNNER_H

#include <chrono>
#include <functional>
#include <queue>

namespace base {

class TaskRunner {
 public:
  using Delay = std::chrono::microseconds;
  
  // Post a task to be run instantly.
  void post_task(const std::function<void()>& f);

  // Post a task to be run after a specified delay.
  void post_delayed_task(const std::function<void()>& f, Delay duration);

  // Run tasks until the task queue is exhausted.
  void run();

  // 
  void shutdown();

  // Return number
  int size() { return tasks_.size() + delayed_tasks_.size(); }

 private:
  using Clock = std::chrono::steady_clock;
  using TimePoint = Clock::time_point;

  class Task {
   public:
    Task(std::function<void()> f);
    void run() const;

   private:
    std::function<void()> f_;
  };

  class DelayedTask : public Task {
   public:
    DelayedTask(std::function<void()> f, Delay delay);
    
    // Returns time until expiry.
    TimePoint expires() const { return expires_; }

    bool operator<(const DelayedTask& other) const;
    
   private:
    TimePoint expires_;
  };

  Delay get_delay_until_next_task();
  bool run_next_task();

  bool running_ = false;
  std::queue<Task> tasks_;
  std::priority_queue<DelayedTask> delayed_tasks_;
};

}

#endif  // TASKS_TASK_RUNNER_H
