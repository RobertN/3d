#pragma once

#include <memory>
#include "third_party/vmmlib/include/vmmlib/matrix.hpp"

class GLProgram;
class InputManager;

class Camera {
  public:
    Camera() {}
    Camera(std::shared_ptr<GLProgram> gl_program);

    void update();

    void move_forward();
    void move_backward();
    void move_left();
    void move_right();
    void rotate_left();
    void rotate_right();

  private:
    void set_direction(const vmml::vec3f& direction);

    std::shared_ptr<GLProgram> gl_program_;
    vmml::mat4f world_to_view_;
    vmml::vec3f position_;
    vmml::vec3f direction_;
    vmml::vec3f up_;
    vmml::vec3f left_;
    float rotation_y_;
};
