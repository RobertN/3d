#ifndef OBJECT_H_
#define OBJECT_H_

#include "third_party/vmmlib/include/vmmlib/matrix.hpp"
#include "third_party/vmmlib/include/vmmlib/vector.hpp"

class RenderObject;

/** Traversal object passed to objects being traversed.
 *
 * The matrix tranform is updated so it always relative to world. Currently only
 * supports traversal of RenderObjects but can be extended to do different kinds
 * traversals simultaneously.
 */
class TraverseObject {
 public:
  TraverseObject();

  class RenderDelegate {
   public:
    virtual void render(RenderObject* render_object, const vmml::mat4f& transform) = 0;
  };

  void set_transform(const vmml::mat4f& transform) { transform_ = transform; }
  const vmml::mat4f& transform() const { return transform_; }

  void enable_render(RenderDelegate* delegate) { render_delegate_ = delegate; }
  RenderDelegate* render_delegate() { return render_delegate_; }

 private:
  RenderDelegate* render_delegate_;
  vmml::mat4f transform_;
};


// Base class of all object in the world.
class Object {
 public:
  Object();

  virtual ~Object();

  // TODO: Should the object own it's childen?
  void add_child(Object* child);
  void remove_child(Object* child);

  void rotate(float scalar, const vmml::vec3f& axis);
  void move(const vmml::vec3f& pos);
  void scale(float scalar);

  vmml::mat4f& get_transform_() const;

  void traverse(TraverseObject& t);

 protected:
  // Called when and object is reached in a traverse.
  // traverse.transforms() is relative to world here.
  virtual void on_traverse(TraverseObject& traverse) = 0;

 private:
  Object* parent_;
  std::vector<Object*> child_objects_;

  // Transform relative to parent.
  vmml::mat4f transform_;
};

#endif
