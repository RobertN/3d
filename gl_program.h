#pragma once

#include <memory>
#include "base/tasks/task_runner.h"
#include "gl.h"
#include "third_party/vmmlib/include/vmmlib/matrix.hpp"

class GLProgram;


class GLProgram {
public:
  static std::shared_ptr<GLProgram> Create(base::TaskRunner& task_runner);

  virtual ~GLProgram() {}
  virtual bool add_shader(GLuint type, const char* shader_filename) = 0;
  virtual void construct() = 0;

  virtual GLuint gl_loc_position() const = 0;
  virtual GLuint gl_normal_position() const = 0;

  virtual void set_projection_matrix(const vmml::mat4f& m) = 0;
  virtual void set_world_to_view_matrix(const vmml::mat4f& m) = 0;
  virtual void set_model_to_world_matrix(const vmml::mat4f& m) = 0;
};
