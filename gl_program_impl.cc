#include "gl_program_impl.h"

#include <assert.h>
#include <sys/stat.h>
#include <fstream>
#include <iostream>
#include <memory>
#include <unordered_map>
#include "base/tasks/task_runner.h"
#include "gl.h"
#include "gl_program.h"

namespace {
time_t get_mtime(const std::string& path) {
  struct stat s;
  if (stat(path.c_str(), &s) == 0)
    return s.st_mtime;
  return 0;
}
}

// static
std::shared_ptr<GLProgram> GLProgram::Create(base::TaskRunner& task_runner) {
  return std::shared_ptr<GLProgram>(new GLProgramImpl(task_runner));
}

GLProgramImpl::GLProgramImpl(base::TaskRunner& task_runner)
    : gl_program_(-1)
    , gl_loc_position_(-1)
    , gl_normal_position_(-1)
    , task_runner_(task_runner) {
  check_for_changes();
}

bool GLProgramImpl::add_shader(GLuint type, const char* shader_filename) {
  assert(installed_shaders_.find(type) == installed_shaders_.end());
  installed_shaders_[type] = ShaderInfo{shader_filename};
  return true;
}

void GLProgramImpl::construct() {
  gl_program_ = glCreateProgram();
  for (std::pair<const GLuint,ShaderInfo>& shader : installed_shaders_) {
    ShaderInfo& info = shader.second;
    if (info.compiled_shader == 0) {
      char* shader_data;
      assert(read_file(info.path, &shader_data));
      std::cout << "Compiling: " << info.path << std::endl;
      info.compiled_shader = compile_shader(shader.first, shader_data);
    }
    if (info.compiled_shader) {
      glAttachShader(gl_program_, info.compiled_shader);
      info.mtime = get_mtime(info.path);
    }
  }
  glLinkProgram(gl_program_);
  glUseProgram(gl_program_);

  vmml::mat4f ident;
  identity(ident);
  set_projection_matrix(ident);
  set_world_to_view_matrix(ident);
  set_model_to_world_matrix(ident);

  // Retrieve references to the variables in the shaders.
  gl_loc_position_ = glGetAttribLocation(gl_program_, "in_Position");
  gl_normal_position_ = glGetAttribLocation(gl_program_, "in_Normal");
}

bool GLProgramImpl::read_file(const std::string& filename, char** output) const {
  assert(output);
  std::ifstream is(filename);
  if (!is)
    return false;

  // Retrieve the size of the file.
  is.seekg(0, std::ios_base::end);
  int32_t length = is.tellg();
  is.seekg(0, std::ios_base::beg);

  // Read the entire file to |output|.
  *output = new char[length + 1];
  (*output)[length] = '\0';
  is.read(*output, length);

  return true;
}

void GLProgramImpl::print_shader_info(GLuint obj) const {
  GLint log_length = 0;
  glGetShaderiv(obj, GL_INFO_LOG_LENGTH, &log_length);

  if (log_length == 0)
    return;

  char* log = new char[log_length];
  GLsizei read;
  glGetShaderInfoLog(obj, log_length, &read, log);
  if (read > 0)
    std::cerr << log << std::endl;
  delete[] log;
}

GLuint GLProgramImpl::compile_shader(GLuint type, const char* shader) const {
  GLuint gl_shader = glCreateShader(type);
  glShaderSource(gl_shader, 1, &shader, NULL);
  glCompileShader(gl_shader);
  print_shader_info(gl_shader);
  return gl_shader;
}

void GLProgramImpl::check_for_changes() {
  bool was_modified = false;
  for (std::pair<const GLuint,ShaderInfo>& shader : installed_shaders_) {
    ShaderInfo& info = shader.second;
    if (get_mtime(info.path) > info.mtime) {
      glDetachShader(gl_program_, info.compiled_shader);
      glDeleteShader(info.compiled_shader);
      info.compiled_shader = 0;
      was_modified = true;
    }
  }

  if (was_modified) {
    glDeleteProgram(gl_program_);
    construct();
  }

  task_runner_.post_delayed_task(
      std::bind(&GLProgramImpl::check_for_changes, this),
      std::chrono::seconds(1));
}
