#pragma once

#include <functional>
#include "base/tasks/task_runner.h"

class InputManager {
  public:
    static InputManager* Create(base::TaskRunner& task_runner);

    virtual ~InputManager() {}
    virtual void add_key_handler(int32_t key, std::function<void(void)>) = 0;
    virtual void add_event_handler(int32_t event_type, std::function<void(void)>) = 0;
};
