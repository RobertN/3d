#include "object.h"

TraverseObject::TraverseObject() {
  identity(transform_);
}

Object::Object() {
  identity(transform_);
}

Object::~Object() {
  // for (Object* o : child_objects_)
  //   delete o;
}

void Object::rotate(float scalar, const vmml::vec3f& axis) {
  vmml::mat4f m;
  m.rotate(scalar, axis);
  transform_ *= m;
}

void Object::move(const vmml::vec3f& pos) {
  vmml::mat4f m;
  identity(m);
  m.set_translation(pos);
  transform_ *= m;
}

void Object::scale(float scalar) {
  transform_.scale(scalar, scalar, scalar);
}

void Object::add_child(Object* child) {
  child->parent_ = this;
  child_objects_.push_back(child);
}

void Object::traverse(TraverseObject& to) {
  vmml::mat4f old_transform = to.transform();

  to.set_transform(to.transform() * transform_);

  on_traverse(to);
  
  for (Object *child : child_objects_) {
    child->traverse(to);
  }

  to.set_transform(old_transform);
}
