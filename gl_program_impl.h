#pragma once

#include "base/tasks/task_runner.h"
#include "gl_program.h"
#include <unordered_map>
#include "base/tasks/task_runner.h"

class GLProgramImpl : public GLProgram {
 public:
  GLProgramImpl(base::TaskRunner& task_runner);

  // Implementation of GLProgram
  bool add_shader(GLuint type, const char* shader_filename);
  void construct();

  GLuint gl_loc_position() const { return gl_loc_position_; }
  GLuint gl_normal_position() const { return gl_normal_position_; }

  void set_projection_matrix(const vmml::mat4f& m) {
    GLuint l = glGetUniformLocation(gl_program_, "Projection");
    glUniformMatrix4fv(l, 1, GL_FALSE, m);
  }

  void set_world_to_view_matrix(const vmml::mat4f& m) {
    GLuint l = glGetUniformLocation(gl_program_, "WorldToView");
    glUniformMatrix4fv(l, 1, GL_FALSE, m);
  }

  void set_model_to_world_matrix(const vmml::mat4f& m) {
    GLuint l = glGetUniformLocation(gl_program_, "ModelToWorld");
    glUniformMatrix4fv(l, 1, GL_FALSE, m);
  }

 private:
  struct ShaderInfo {
    std::string path;
    time_t mtime = 0;
    GLuint compiled_shader = 0;
  };

  bool read_file(const std::string& filename, char** output) const;
  void print_shader_info(GLuint obj) const;
  GLuint compile_shader(GLuint type, const char* shader) const;
  void check_for_changes();

  std::unordered_map<GLuint,ShaderInfo> installed_shaders_;
  GLuint gl_program_;
  GLuint gl_loc_position_;
  GLuint gl_normal_position_;
  base::TaskRunner& task_runner_;
};
