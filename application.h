#pragma once

#include <memory>
#include "base/tasks/task_runner.h"
#include "camera.h"
#include "gl.h"
#include "gl_program.h"
#include "object.h"
#include "render_object.h"
#include "track.h"

class InputManager;
class RenderObject;
class SDL_Window;

class Application :
  TraverseObject::RenderDelegate {
  public:
    Application();

    bool init();
    void main_loop();

    // TraverseObject::RenderDelegate implementation
    void render(RenderObject* render_object, const vmml::mat4f& transform);

    // TODO: Move globals somewhere else?
    static base::TaskRunner* main_task_runner() { return &task_runner_; }

  private:
    void setup_opengl();
    void run_frame();
    void terminate();

    void setup_track();

    std::shared_ptr<SDL_Window> window_;
    int16_t width_;
    int16_t height_;

    bool running_;

    std::shared_ptr<GLProgram> program_;
    std::vector<std::unique_ptr<RenderObject> > objects;

    static base::TaskRunner task_runner_;
    std::shared_ptr<InputManager> input_manager_;
    std::unique_ptr<Camera> camera_;
    std::unique_ptr<Track> track_;
};
